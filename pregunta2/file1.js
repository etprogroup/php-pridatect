
function SpecialSum(k, n){
    var value = 0;

    if(k > 0) {
        for (var i = 1; i <= n; i++) {
            var newSum = SpecialSum(k - 1, i);
            value = value + newSum;
        }
    }else{
        value = n;
    }

    return value;
}