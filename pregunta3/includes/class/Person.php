<?php

class Person{
    private $pets = array();

    function __construct(){
        $this->pets = array();
    }

    public function addPet($pet){
        $newPet = array(
            $pet->getName() => $pet->getType()
        );

        $this->pets = array_merge($this->pets, $newPet);
    }

    public function getPets(){
        return $this->pets;
    }
}